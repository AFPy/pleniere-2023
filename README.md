# Plénière d'ouverture 
## PyConFr 2023

Slides des outils de l'asso, partenaires, et sponsors, présentées lors
de la PyConFr 2023.

Pour obtenir les slides en HTML :

    pip install mdtoreveal
    mdtoreveal ouverture.md
    see ouverture.html


## TODO

Ajouter une slide sur la diversité et une slide sur le CoC.

