# Hello world

<img src="static/logo.svg" style="width: 80%" />

# L'asso

<img src="static/BigBlueButton_logo.svg" style="width: 90%" />

https://bbb.afpy.org

##  

<img src="static/gitea.svg" style="width: 60%" />

https://git.afpy.org

##  

<img src="static/discourse.png" style="width: 100%" />

https://discuss.afpy.org

# Les partenaires

<img src="static/Université%20de%20Bordeaux.svg" style="width: 80%" />

##  

<img src="static/Gandi.svg" style="width: 80%" />

##  

<img src="static/Marie-Ange%20Vollard.svg" style="width: 80%" />

##  

<img src="static/CourtBouillon.svg" style="width: 75%" />


# Sponsors

<style>
h2 {display: none}
body {
    background: url("static/2023-snakes.svg") no-repeat !important;
    background-size: cover !important;
} 
</style>


<img src="static/Python%20Software%20Foundation.svg" style="width: 80%" />

##  

<img src="static/Taipy.svg" style="width: 80%" />

##  

<img src="static/Alma.svg" style="width: 80%" />

##  

<img src="static/Clever%20Cloud.svg" style="width: 80%" />

##  

<img src="static/GitGuardian.svg" style="width: 80%" />




##  

<img src="static/CGWire.svg" style="width: 80%" />

##  

<img style="width:70%;" src="static/Entr'ouvert.svg"></img>

##  

<img style="width:70%;" src=static/Makina%20Corpus.svg></img>

##  

<img src="static/Michelin.svg" style="width: 100%" />




##  

<img src="static/Alkivi.svg" style="width: 80%" />


##  

<img src="static/Camptocamp.svg" style="width: 80%" />



##  

<img src="static/Emencia.svg" style="width: 100%" />


##  

<img src="static/Mergify.svg" style="width: 80%" />


##  

<img src="static/Mozilla.svg" style="width: 80%" />


##  

<img src="static/MyBeezBox.svg" style="width: 80%" />


##  

<img src="static/Yaal%20Coop.svg" style="width: 80%" />


# Le bla bla

<img src="static/logo.svg" style="width: 80%" />
